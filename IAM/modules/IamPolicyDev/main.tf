provider "aws" {
  region = "us-east-2"
}

#-----------------------------------------------
# GENERATE AN IAM POLICY DOCUMENT IN JSON FORMAT
#-----------------------------------------------
data "aws_iam_policy_document" "demo" {
 #- Deny Creating/rebuild/Deleting -#
  statement {
    effect  = "Deny"
    actions = ["elasticbeanstalk:CreateEnvironment",
               "elasticbeanstalk:RebuildEnvironment",
               "elasticbeanstalk:TerminateEnvironment"
                    ]
    resources = ["*"] 
 }
  #- Allow Machines to be created of the type T2.micro/Small $in the specific Subnet  -#
  statement {
    effect        = "Allow"
    actions       = ["ec2:RunInstances"]
    resources     = [
      "arn:aws:ec2:us-east-2:964436022767:instance/*",
      "arn:aws:ec2:us-east-2:964436022767:subnet/subnet-0ba82347",
      #arn:partition:service:region:account-id:resourcde-type/resource-id#
      ]
    condition {
      test = "StringEquals"
      variable ="ec2:InstanceType"
      values = ["t2.micro","t2.small"]
   }
 }
}
#----------------------
# CREATE AN IAM POLICY
#----------------------
resource "aws_iam_policy" "dev" {
  name = "dev_policy"
  path = "/"
  policy = data.aws_iam_policy_document.demo.json
}
