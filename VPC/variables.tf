variable "vpc_cidr" {
 default = "10.0.0.0/16"
}

variable "subnet1_cidr" {
  default = "10.0.0.0/24"
}

variable "subnet1_az" {
 default = "us-east-2b"
}

variable "subnet2_cidr" {
  default = "10.0.1.0/24"
}

variable "subnet2_az" {
 default = "us-east-2a"
}
