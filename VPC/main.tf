provider "aws" {
  region = "us-east-2"
}

#---------------
# Create a VPC
#---------------

resource "aws_vpc" "my_vpc" {
  cidr_block  = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "DEMO VPC"
 }
}

#----------------------------------------
# create a public subnet on AZ us-east-2b
#----------------------------------------

resource "aws_subnet" "public_us_east_2a" {
  vpc_id   = aws_vpc.my_vpc.id
  cidr_block = var.subnet1_cidr
  availability_zone = var.subnet1_az

  tags = {
    Name = "Public Subnet us-east-2b"
  }
}

#----------------------------------------
# create a public subnet on AZ us-east-2a
#----------------------------------------

resource "aws_subnet" "public_us_east_2b" {
  vpc_id   = aws_vpc.my_vpc.id
  cidr_block = var.subnet2_cidr
  availability_zone = var.subnet1_az

  tags = {
    Name = "Public Subnet us-east-2b"
  }
}

#-----------------------------------
# Create an IGW for your new VPC
#----------------------------------

resource "aws_internet_gateway" "my_vpc_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "DEMO VPC - Internet Gateway"
 }
}

#---------------------------------
#Create an Routable for your VPC
#---------------------------------

resource "aws_route_table" "my_vpc_public" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_vpc_igw.id
 }

  tags = {
   Name = "Public Subnets Route Table for DEMO VPC"
 }
}

#------------------------------------------------------------
# Associate the RouteTable to the Subnet created at us-east-2a
#-------------------------------------------------------------
resource "aws_route_table_association" "my_vpc_us_east_2a_public" {
  subnet_id = aws_subnet.public_us_east_2a.id
  route_table_id =aws_route_table.my_vpc_public.id
}

#------------------------------------------------------------
# Associate the RouteTable to the Subnet created at us-east-2b
#-------------------------------------------------------------
resource "aws_route_table_association" "my_vpc_us_east_2b_public" {
  subnet_id = aws_subnet.public_us_east_2b.id
  route_table_id =aws_route_table.my_vpc_public.id
}

